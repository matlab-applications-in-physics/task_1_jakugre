% Matlab R2020b script
% name: moj_kalkulator.m
% author: jakugre
% date: 2020-11-13
% version: v1.5


%Task 1
%Defining planck constant
h = 6.62607015e-34;% Planck's constant is in J*S
result_1 = h/(2*pi) %Dirac's constant is in J*s




%Task 2
%Defining euler's number 
e = exp(1);
%45 degrees is equal to pi/4
result_2 = sin(pi/(e*4))



%Task 3
%Converting hexadecimal number to decimal number
number = hex2dec('0x0098d6')
result_3 = number/1.445e23



%Task 4
result_4 = sqrt(e-pi)



%Task 5
%Converting pi to string so 12th decimal number can be printed
pi_str = num2str(pi,15);
result_5 = pi_str(14)



%Task 6
%Stating date of birth
birth_date = 'June 28, 2000 20:30:00';

%Converting date of birth to proper date format
time_1 = datevec(birth_date, 'mmmm dd, yyyy HH:MM:SS');

%Stating current date
time_2 = clock;

%Calculating time difference in seconds 
time_diff = etime(time2,time1); %timeDiff is in seconds

%Converting secodns to hours
time_diff_inIhours = time_diff/3600


%Task 7
%Defining Earth Radius
earth_radius = 6.371e5; %Radius is in m
result_7 = atan(e^(sqrt(7)/2-log((earth_radius/1e8)))/hex2dec('0xaaff'))



%Task 8
%Amount of particles in one mol
n = 6.02214076e23;
n1 = n*1e-6;
atoms_in_alcohol = (n1*9)/4



%Task 9
atoms_of_C = n*2;
atoms_of_C_13 = atoms_of_C/101;
per_mille_of_c_13 = (atoms_of_C_13/(n1*9))*1000



